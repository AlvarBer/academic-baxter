#!/usr/bin/env python
"""
Baxter draughts program
Baxtor actions module
M L Walters, July 2014

Problems:
    Misses jumps in: get_move_visual()?
    Minor issue: Can miss out intermediate move if close to navpos
"""

from __future__ import print_function
try:
    input = raw_input
except:
    pass


# Standard libs
from math import pi
import pickle
#import numpy as np
from time import sleep
from os import system
import logging
logger = logging.getLogger(__name__)
# Standard ROS libs
import rospy
import roslib
import std_msgs.msg # Necessary?
import tf
import geometry_msgs.msg
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2

# Baxter libs
import baxter_interface
#from baxter_core_msgs.msg import ITBState
from moveit_commander import conversions
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
    )
from std_msgs.msg import Header
from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
    )
import BaxUI

# module variables
listener = None # Global access to ROS TF node
home_joint_angles = None # Left arm home/camera position
board_dict = None
#occupancy_dict ={}
zOffset = 0
zApproach = 0.1
takeq = 0

current_image = None
BOARD_LANES = 8

def move_piece (startpos, endpos, limb="left"):
    """
    Will move a game piece from location on the game board. The positions can be
    given by either:
        A string of the form "<C>n", where <C> is a letter from A to H, n is a
        number from 1 to 8
    The limb string may be either "left" or "right" and will use the right or
    left arm respectively??
    """
    if startpos in board_dict and endpos in board_dict:
        print("Moving piece from ", startpos, " to ", endpos, ".")
        say("Moving piece from "+ startpos+ " to "+ endpos+".")
        move_arm_xyz("left", board_dict[startpos]+[zApproach])
        move_arm_xyz("left", board_dict[startpos]+[zOffset])
        # Gripper not work!!!!!!
        baxter_interface.Gripper('left').close()
        rospy.sleep(.3)
        move_arm_xyz("left", board_dict[startpos]+[zApproach])
        # Extra move here to avoid swiping board
        move_arm_xyz("left", board_dict[endpos]+ [zApproach])
        move_arm_xyz("left", board_dict[endpos]+[zOffset])
        baxter_interface.Gripper('left').open()
        rospy.sleep(.3)
        move_arm_xyz("left", board_dict[endpos]+[zApproach])
    else:
        print(""""positions should be a string with a board position
        in the form Cn, or some other learned place name.""")
    return

def moveTo(placeName=None, approach = 0, limb = "left"):
    if placeName in board_dict:
        move_arm_xyz(limb, board_dict[placeName] + [approach])


def take_piece(takepos, colour="black", limb="left"):
    """ Will take a piece at takepos, and put in the take area relevant to the
    player colour, "Black" or "White".
    Parameter limb may be either "left" or "right"
    """
    global takeq
    print("Taking piece for ", colour, " player.")
    move_arm_xyz("left", board_dict[takepos]+[zApproach])
    move_arm_xyz("left", board_dict[takepos]+[zOffset])
    # Close Gripper
    baxter_interface.Gripper('left').close()
    rospy.sleep(.3)
    move_arm_xyz("left", board_dict[takepos]+[zApproach])
    # Move to next free slot in take area
    move_arm_xyz("left", board_dict["take"+str(takeq)]+[zApproach], [pi, 0, pi * 3.0/4.0])
    move_arm_xyz("left", board_dict["take"+str(takeq)]+[zOffset+0.002], [pi, 0, pi * 3.0/4.0])
    baxter_interface.Gripper('left').open()
    rospy.sleep(.2)
    move_arm_xyz("left", board_dict["take"+str(takeq)]+[zApproach], [pi, 0, pi * 3.0/4.0])
    takeq += 1

def move_home(limb = "left"):
    """ Moves baxters "left" or "right" arms to Home position, so that camera
    in "left" or "right" arm can view the board from above. The free arm moves
    to a position where the human player can use the arm mounted controls
    """
    global board_dict, home_joint_angles
    print("Moving to Home position")

    if home_joint_angles == None:
        # Avoid using IK as returns awkward postures
        print("Moving to campos")
        logger.info('Moving to campos')
        move_arm_xyz("left", board_dict["campos"])
        home_joint_angles = baxter_interface.Limb('left').joint_angles()
    else:
        print("Moving to home_joint_angles position")
        print(home_joint_angles)
        baxter_interface.Limb('left').move_to_joint_positions(home_joint_angles)
    sleep(1.0) # allow position to settle for camera

def move_navpos():
    print("Moving right arm for easy access to Navigator")
    say("Moving right arm for easy access to Navigator")
    """#### Misss this move out if close to navpos
    baxter_interface.Limb('right').move_to_joint_positions({'right_s0':0.785,
                                                             'right_s1':-0.517,
                                                             'right_w0':-0.575,
                                                             'right_w1':1.832,
                                                             'right_w2':0.531,
                                                             'right_e0':0.311,
                                                             'right_e1':1.287
                                                             })
    """
    baxter_interface.Limb('right').move_to_joint_positions({'right_s0':0.036,
                                                            'right_s1':0.309,
                                                            'right_w0':-1.535,
                                                            'right_w1':2.094,
                                                            'right_w2':1.035,
                                                            'right_e0':0.680,
                                                            'right_e1':1.007
                                                            })

def calibrate_board():
    """ Sets up the robot so that it knows the board position. Settings are
    saved to a file so should only need calibrating if the workspace changes.
    Manually move the arm and centalise the gripper over the bottom left square (a0),
    then press RETURN. Then manually move the arm and centalise the gripper over
    the top right square (h7), then press RETURN.
    """
    global board_dict, zOffset, zApproach, listener
    print("Calibrating:")
    #check if gripper is calibrated, if not, reboot it and calibrate
    if not baxter_interface.Gripper('left').calibrated():
        #print "cal"
        #baxter_interface.Gripper('left').reboot()
        baxter_interface.Gripper('left').calibrate()

    # get A0 position
    print("Manually centalise left gripper over A0 square.")
    msg = input("Press RETURN")
    try:
        trans, rot = listener.lookupTransform('/base', '/left_gripper',
                                               rospy.Time(0))
    except (tf.LookupException, tf.ConnectivityException,
            tf.ExtrapolationException):
        pass#raise #continue
    #print("trans A0 = ", trans, "rot A0 = ", rot)
    a0 = [trans[0], trans[1]]
    zA0 = trans[2]
    print("Position A0 = ", a0)

    # Get H7 position
    print("Manually centalise left gripper over H7 square.")
    msg = input("Press RETURN")
    try:
        trans, rot = listener.lookupTransform('/base', '/left_gripper',
                                               rospy.Time(0))
    except (tf.LookupException, tf.ConnectivityException,
            tf.ExtrapolationException):
        pass#raise #continue
    #print "Position H7 = (", trans ,"), (", rot, ")"
    h7 = [trans[0], trans[1]]
    zH7 = trans[2]
    print("Position H7 = ", h7)

    # Work out board positions
    if abs(zA0 - zH7) > 0.05: # May need to adjust this (0.05m = 5mm) for consistant operation
        print("Board not level")
        return
    else:
        print("Board level witnin limits")
        zOffset = zA0 - 0.005 + (zA0 - zH7) // 2
    print("Pick height  = ", zOffset, zA0, zH7)
    board_dict["campos"]=[0,0,0]
    board_dict["campos"][0] = a0[0] - 0.080 # 0.070 # x position
    board_dict["campos"][1] = a0[1] + 0.440 # 0.440 # y position
    board_dict["campos"][2] = zOffset + 0.462
    print("Camera position = ", board_dict["campos"])
    board_dict["zoffset"] = zOffset

    # Calculate board xy positions
    # A0 position
    x = a0[0]
    y = a0[1]
    # H7 position
    hx = h7[0]
    hy = h7[1]
    print("A0 = (",x, y, ")")
    dx = (x - hx)/7
    dy = (y - hy)/7
    print("dx, dy = ", dx, dy)
    for c in "ABCDEFGH":
        for n in "01234567":
            board_dict[c+n] = [x,y]
            x = x - dx
            print(c+n, board_dict[c+n])
        x = board_dict["A0"][0]
        y = y - dy

    # Calculate take storage positions
    x = board_dict["H7"][0]
    y = board_dict["H7"][1] + 0.1
    # print "Base take position = ", x, y
    for takeno in range(0, 5):
        board_dict["take" + str(takeno)] = [x,y]
        x = x + 0.06
        #print takeno, x,y
    y = y + 0.06
    x = board_dict["H7"][0]
    for takeno in range(5, 9):
        board_dict["take" + str(takeno)] = [x,y]
        x = x + 0.06
    y = y + 0.06
    x = board_dict["H7"][0]
    for takeno in range(9, 13):
        board_dict["take" + str(takeno)] = [x,y]
        x = x + 0.06
    #print board_dict
    # Save new positions to board.cfg
    print("Saving cfg file")
    pickle.dump(board_dict, open("board.cfg", "wb"))
    print("Saved game configuration to ./src/baxter_game/board.cfg")

def move_arm_xyz(limb ="left", xyz =[0.67, 0.25, 0.27], rot = [pi, 0, pi]):
    #try:
    pose = xyz+rot
    angles = ik_solver_request(limb, pose)
    #print angles
    if angles != None:
        print("Moving to position") #, pose
        baxter_interface.Limb(limb).move_to_joint_positions(angles)
    #except:
    #    print "Out of range"

def ik_solver_request(input_limb, input_pose):
    print('Going to ik with {}, {}'.format(input_limb, input_pose))
    logger.info('IK solver request')
    #input error checking
    if len(input_pose) == 6:
        quaternion_pose = conversions.list_to_pose_stamped(input_pose, "base")
    elif len(input_pose) == 7:
        quaternion_pose = input_pose
    else:
        logger.warning("""Invalid Pose List:
                Input Pose to function: ik_solver_request must be a list of:
                6 elements for an RPY pose, or
                7 elements for a Quaternion pose""")
        return None

    if input_limb == "right" or input_limb == "left":
        limb = input_limb
    else:
        logger.warning("""Invalid Limb:
            Input Limb to function: ik_solver_request must be a string:
            'right' or 'left'""")
        return None
    #request/response handling
    #print quaternion_pose
    node = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"
    ik_service = rospy.ServiceProxy(node, SolvePositionIK)
    ik_request = SolvePositionIKRequest()
    ik_request.pose_stamp.append(quaternion_pose)
    try:
        rospy.wait_for_service(node, 5.0)
        ik_response = ik_service(ik_request)
    except (rospy.ServiceException, rospy.ROSException), error_message:
        raise
        rospy.logerr("Service request failed: %r" % (error_message,))
    if ik_response.isValid[0]:
        print("PASS: Valid joint configuration found")
        #convert response to JP control dict
        limb_joints = dict(zip(ik_response.joints[0].name, ik_response.joints[0].position))
        return limb_joints
    else:
        print("FAILED: No valid joint configuration for this pose found")
        return None

def init():
    global listener, board_dict, zOffset, zApproach, previous_image
    # We load the manifest of the package we are in
    roslib.load_manifest('baxter_game')
    rospy.init_node('baxter_game')
    listener = tf.TransformListener()
    BaxUI.reset_display() # Blanks BaxUI display
    # Set up board
    # Inititialise board to game translation table
    # Translates "Cn" type cords to xyz coords (nn)

    # print board [7][7] # debug

    # If "board.cfg" file exists, load it
    # board_dict contains robot cords for pick positions etc.
    try:
        print("Looking for board.cfg file.")
        # with statement
        board_dict = pickle.load(open("board.cfg", "rb"))
        print("Successfully loaded board.cfg")
    except:
        print("File board.cfg not found, please calibrate_board")
        board_dict = {}
        calibrate_board()

    if "zoffset" in board_dict: # z level of board i.e. pick height, z cord
        zOffset = board_dict["zoffset"]
    else:
        zOffset = 0.0
    zApproach = zOffset + 0.1 # pick approach height - hardcoded
    if "campos" not in board_dict:
        board_dict["campos"] = [0.645, 0.377, 0.332]
    else:
        print("campos loaded", board_dict["campos"])

    #check if gripper is calibrated, if not, reboot it and calibrate
    if not baxter_interface.Gripper('left').calibrated():
        print("Calibrating left gripper")
        #baxter_interface.Gripper('left').reboot()
        baxter_interface.Gripper('left').calibrate()
    # % holding force (0% - 100%)
    baxter_interface.Gripper('left').set_holding_force(10)
    baxter_interface.Gripper('left').set_moving_force(10)
    baxter_interface.Gripper('left').open()

    # Initialise the left hand camera to view board
    rh_camera_sub = rospy.Subscriber("/cameras/left_hand_camera/image", Image,
                                     update_image)
    move_home("left")
    move_navpos()
    previous_image = get_image()

def update_image(data):
    """ Callback for updating/displaying image from cams
    """
    global current_image
    #callback to update image stream from camera
    logger.info('Getting image from left arm cam')
    im_grey = CvBridge().imgmsg_to_cv2(data, 'mono8') # "bgr8") # for colour
    im_grey = im_grey[100:380, 360:640]
    thresh, im_bw = cv2.threshold(im_grey, 140, 255, cv2.THRESH_BINARY |
                                                     cv2.THRESH_OTSU)
    # cut out ROI - draughts board
    #cv2.namedWindow("BW Image", cv2.CV_WINDOW_AUTOSIZE) # Create a window for display.
    #cv2.imshow("BW Image", im_bw)
    #cv2.waitKey(3) # Throws warning if 3ms wait not present
    current_image = im_bw
    cv2.imshow('Gray image', im_grey)
    cv2.waitKey(3)
    color_cropped = CvBridge().imgmsg_to_cv2(data, 'bgr8')[400:, :]
    cv2.imshow('Color image', color_cropped)
    print(color_cropped.shape)
    cv2.waitKey(3)

def get_image():
    global current_image, fileNo
    #process image
    # Auto threshold
    im_bw = None
    while im_bw == None: # keep trying as update_image runs in background
        try:
            im_bw = current_image.copy()
        except:
            pass
    return im_bw

def get_move_visual(pre_image, post_image, ours_value=60, others_value=20):
    """ Given two images it compares both to find the move along the removed
    pieces (if any).

    The ours and others value refer to the current pieces we are looking at
    that have jump, and the others wich have been captured

    It returns either the move or None if no move was done.
    """
    im_xor = cv2.bitwise_xor(post_image, pre_image)
    im_start = cv2.bitwise_and(im_xor, pre_image)
    im_end = cv2.bitwise_and(im_xor, post_image)

    if __debug__:
        cv2.imshow("Before Image", im_start)
        cv2.imshow("After Image", im_end)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    board_size = im_xor.shape[0]
    tile_size = board_size // 8
    tile_edges = range(0, board_size, tile_size)
    captured = []
    for x_idx, x_coord in enumerate(tile_edges):
        for y_idx, y_coord in enumerate(tile_edges):
            tile_at_start = im_start[y_coord: y_coord + tile_size,
                                     x_coord: x_coord + tile_size].mean()
            tile_at_end = im_end[y_coord: y_coord + tile_size,
                                 x_coord: x_coord + tile_size].mean()
            if __debug__:
                print('({}, {}) at start: {} | at end: {}'.format(x_idx, y_idx,
                                                                  tile_at_start,
                                                                  tile_at_end))
            if ours_value - 10 < tile_at_start < ours_value + 10:
                start_pos = x_idx, y_idx
            elif ours_value - 10 < tile_at_end < ours_value + 10:
                end_pos = x_idx, y_idx
            elif others_value - 10 < tile_at_start < others_value + 10:
                captured.append((x_idx, y_idx))

    if start_pos and end_pos: # There is a move
        logger.info('Detected move {} -> {}'.format(start_pos, end_pos))
        return pos_to_move(*start_pos), pos_to_move(*end_pos)
    else:  # There has not been a move
        logger.debug('No move was detected')
        return None

moveItem = 0 # menu item selected - integer

def set_move(selected = 0):
    global moveItem  # Callback from BaxUI
    print("Button ", selected, " selected")
    BaxUI.runit = False
    moveItem = selected + 1

def get_move(robot_legal, bax_move=1):
    global moveItem
    #print("Robot Legal Moves")
    BaxUI.set_move = set_move  # set BaxUI callback
    # Show options on navigator
    print("Waiting for navigator response")
    say("Please make your move")
    if bax_move == 1:
        # Baxter moves for player
        index = 0
        mainmenu = [0] * len(robot_legal)
        for m in robot_legal:
            #print str(index + 1)+": "+ str(m)
            mainmenu[index] = [str(robot_legal[index]), "set_move"]
            index += 1
        usr_input = BaxUI.showList(mainmenu)
        #print moveItem
        return usr_input
    else:
        print("Watching board for player to move!")
        #usr_input = integer index to robot_legal selected
        usr_move = False
        usr_input = 0
        while usr_move == False:
            global previous_image
            previous_image = get_image()
            logger.info('Acquired pre move image')
            BaxUI.showList([['Finished move', 'Stop']])
            BaxUI.stop()
            current_image = get_image()
            usr_move = get_move_visual(previous_image, current_image)
            for m in robot_legal:
                if m[0] == usr_move[0] and m[1] == usr_move[1]:
                    print("Legal Move! ", m)
                    return usr_input
                else:
                    usr_input += 1
            print("Illegal Move!")
            usr_move = False
            usr_input = 0

fileNo=0
def save_image(filename="BoardImage"):
    global fileNo
    image = get_image()
    filename= filename + str(fileNo) + ".png"
    fileNo += 1
    cv2.imwrite(filename, image)
    print("Saved to: ", filename, fileNo)

def say(msg="ok"):
    """ Says a phrase. """
    system("espeak '" + msg + "'")

def pos_to_move(x, y):
    """ Given a position translates into a move. """
    global BOARD_LANES
    return 'ABCDEFGH'[x], str(BOARD_LANES - y)

def move_to_pos(x, y):
    """ Given a move translate into a position. """
    global BOARD_LANES
    return 'ABCDEFGH'.find(x), BOARD_LANES - int(y)

if __name__ == "__main__":
    init() # Initialise ROS node (has to be done in main module!)

    #move_home("left")
    #move_piece("A0", "B1")
    #moveTo("take0")

    #calibrate_board() # Only required if robot/table/board is moved
    # Get right arm angles/position for navigator access
    #print baxter_interface.Limb('right').joint_angles()
    move_navpos()
    move_home("left")


    get_image()

    # Interactively test
    msg = ''
    while msg != 'q':
        msg = input("Function to test or 'q' to Quit: ")
        try:
            print(eval(msg))
        except Exception:
            print('Malformed expression')

